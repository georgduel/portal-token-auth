require('dotenv').config();
const crypto = require('crypto');
const querystring = require('querystring');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const app = express();

const PORT = 3334;

const member = {
  id: 1,
  name: 'Daniel Smith',
  email: 'daniel@smith.com',
  points: 50,
  ownerSlug: 'geo',
};

const validNonces = new Set();

app.use(bodyParser.json());
app.use(cors());

app.get('/member', (req, res) => {
  const {member: memberID, slug} = req.query;

  // TODO: Lookup member

  return res.json({ member });
});

app.get('/nonce', (req, res) => {
  const nonce = crypto.randomBytes(16).toString('hex');

  // TODO: Save nonce
  validNonces.add(nonce);
  console.log('/nonce', Array.from(validNonces));

  return res.json({ nonce });
});

app.get('/login', (req, res) => {
  console.log('/login', Array.from(validNonces));
  const { payload, sig } = req.query;

  const hmac = crypto
    .createHmac('sha256', process.env.APP_SECRET)
    .update(payload)
    .digest('hex');
  
  if (hmac !== sig) {
    return res.status(401).send('Signatures don\'t match');
  }

  const {nonce, slug, ...memberData} = querystring.decode(
    Buffer.from(payload, 'base64').toString('ascii')
  );
  if (!validNonces.has(nonce)) {
    return res.status(401).send('Invalid nonce'); 
  }
  validNonces.delete(nonce);

  // TODO: Find or create member

  return res.send(`Logged in ${memberData.name}.`)
});

app.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}`);
});
