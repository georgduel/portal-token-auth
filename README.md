# Token based authentication for the Duel portal (Demo)

## Setup

Add a `.env` file (or load environment variables in a different way). Include these (choose any value):

- APP_SECRET=password

Start both servers with `npm run client` and `npm run server`. `client` represents the eCommerce store, `server` represents the Duel portal.

Head over to `http://localhost:3335` to start the flow.
