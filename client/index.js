require('dotenv').config();
const path = require('path');
const crypto = require('crypto');
const querystring = require('querystring');
const express = require('express');
const app = express();

const PORT = 3335;
const SLUG = 'geo';

app.use(express.static(path.resolve('client', 'public')));

app.get('/', (req, res) => {
  res.sendFile('index.html');
});

const members = [
  {
    id: 1,
    name: 'Daniel Smith',
    email: 'daniel@smith.com',
  },
  {
    id: 2,
    name: 'Peter Smith',
    email: 'peter@smith.com',
  },
];

// NEEDS TO BE PROTECTED
app.get('/duel/signin', async (req, res) => {
  const { nonce } = req.query;
  const payload = Buffer.from(
    querystring.encode({...members[0], nonce, slug: SLUG})
  ).toString('base64');
  const signed = crypto
    .createHmac('SHA256', process.env.APP_SECRET)
    .update(payload)
    .digest('hex');
  const link = `http://localhost:3334/login?payload=${encodeURIComponent(payload)}&sig=${signed}`;

  return res.json({ link });
});

app.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}`);
});
